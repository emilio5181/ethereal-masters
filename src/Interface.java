import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Interface extends JFrame implements ActionListener {
	Jogador jogador;
	Tabuleiro tab;
	AtorJogador aJogador;
	
	//Cria as interfaces
	InterfacePlacar placar;
	InterfaceCarta [] aCartas = {new InterfaceCarta(1),new InterfaceCarta(2),new InterfaceCarta(3),new InterfaceCarta(4),new InterfaceCarta(5)};
	InterfacePosicao [] aPosicoes = new InterfacePosicao[16];
	
	//Variaveis usadas
	boolean cartaSelecionada = false;
	String nomeOutroJogador = "Adversario";
	Carta carta;
	InterfaceCarta b = null;
	
	//tamanho do JFrame
	int x = 1200;
	int y = 850;
	
	//Cria os botoes
	JButton iniciar = new JButton("Iniciar");
	JButton mudarNome = new JButton("Mudar Nome");
	JButton nome = new JButton("Nome Jogador");
	JButton conectar = new JButton("Conectar");
	
	JButton desconectar = new JButton("Desconectar");
	JButton nome2 = new JButton("Jogador Vermelho");
	JButton nome1 = new JButton("Jogador Azul");
	JButton desistir = new JButton("Desistir");
	
	
	//Cria as label com imagens
	ImageIcon imagem1 = new ImageIcon(getClass().getResource("imagens/emJogo.png"));
	JLabel labelImagem1 = new JLabel(imagem1);
	
	ImageIcon imagem = new ImageIcon(getClass().getResource("/imagens/inicial.png"));
	JLabel labelImagem = new JLabel(imagem);
	
	
	
	Interface () {
		super();
		
		//jpanel
		setTitle("Ethereal Masters");
		setSize(x,y);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setLayout(null);
		
		//Define posicoes dos botoes
		desconectar.setBounds(500,0,200,30);
		iniciar.setBounds(100,100,200,30);
		conectar.setBounds(100,300,200,30);
		mudarNome.setBounds(100,500,200,30);
		nome.setBounds(100,700,200,30);
		
		nome2.setBounds(900,70,200,30);
		nome1.setBounds(100,70,200,30);
		desistir.setBounds(900,0,200,30);
		
		//Adiciona os botoes ao Action Listener
		desconectar.addActionListener(this);
		iniciar.addActionListener(this);
		conectar.addActionListener(this);
		mudarNome.addActionListener(this);
		
		desconectar.addActionListener(this);
		desistir.addActionListener(this);
		
		imagem1.setImage(imagem1.getImage().getScaledInstance(x,y,1));
		labelImagem1.setBounds(0, 0, x, y);
		
		imagem.setImage(imagem.getImage().getScaledInstance(x,y,1));
		labelImagem.setBounds(0, 0, x, y);
	}
	
	public void inicio() {
		jogador = new Jogador();
		aJogador = new AtorJogador(this,jogador);
		tab = new Tabuleiro(jogador,aJogador);
		aJogador.adicionarTabuleiro(tab);
		
		interfaceInicial();
	}

	public void interfaceInicial() {
		//remove todos os botoes
		removerCartas();
		removerPosicoes();
		removerPlacar();
		
		remove(iniciar);
		remove(mudarNome);
		remove(nome);
		remove(conectar);
		
		remove(desconectar);
		remove(nome2);
		remove(nome1);
		remove(desistir);
		
		remove(labelImagem1);
		remove(labelImagem);
		
		
		
		nome.setText(jogador.informarNome());
		
		add(iniciar);
		add(conectar);
		add(mudarNome);
		add(nome);
		add(desconectar);
		
		//Imagem de Fundo inicial
		add(labelImagem);
		repaint();
	}
	
	public void interfaceEmJogo(boolean comecou) {
		
		if (comecou) {
			nome2.setBackground(new Color(178,34,34));
			nome1.setBackground(new Color(100,149,237));
		} else {
			nome2.setBackground(new Color(100,149,237));
			nome1.setBackground(new Color(178,34,34));
		}
		
		Placar placarJogo = tab.placar();
		if (placarJogo != null)
			placar = new InterfacePlacar(placarJogo);
		
		//remove todos os botoes
		removerCartas();
		removerPosicoes();
		removerPlacar();
		
		remove(iniciar);
		remove(mudarNome);
		remove(nome);
		remove(conectar);
				
		remove(desconectar);
		remove(nome2);
		remove(nome1);
		remove(desistir);
				
		remove(labelImagem1);
		remove(labelImagem);
		
		nome2.setText(jogador.informarNome());
		nome1.setText(nomeOutroJogador);
		
		add(desconectar);
		add(nome2);
		add(nome1);
		add(desistir);
		addPlacar();
		
		//inicia as interfaces de posicoes de acordo com oq esta no tabuleiro
		iniciarPosicoes();
		adicionarPosicoes();
		
		//inicia as interfaces das cartas de acordo com oq o jogador tem na mao
		iniciarCartas();
		adicionarCartas();
		
		//Imagem de Fundo inicial
		add(labelImagem1);
		
		repaint();
	}

	private void adicionarCartas() {
		remove(labelImagem1);
		for (InterfaceCarta a : aCartas) {
			if (a != null)
				a.adicionarBotao(this);
		}
		add(labelImagem1);
		repaint();
	}

	private void iniciarCartas() {
		Carta [] cartas = jogador.informarCartas();
		int tamanho = cartas.length;
		for (int i = 0; i < tamanho ; i++) {
			aCartas[i].adicionarCarta(cartas[i]);
			aCartas[i].adicionarListener(this);
		}
	}

	private void iniciarPosicoes() {
		int tamanho = aPosicoes.length;
		removerPosicoes();
		for(int i = 0; i < tamanho ; i++) {
			aPosicoes[i] = new InterfacePosicao(tab.posicoes[i]);
			aPosicoes[i].adicionarListener(this);
		}		
	}

	private void addPlacar() {
		remove(labelImagem1);
		if (placar != null)
			placar.adicionarPlacar(this);
		add(labelImagem1);
		repaint();
	}

	//Remove todos os botoes das cartas
	private void removerCartas() {
		remove(labelImagem1);
		for (InterfaceCarta a : aCartas) {
			if (a != null)
				a.removerBotao(this);
		}
		add(labelImagem1);
		repaint();
	}
	
	//remove todos os botoes das posicoes
	private void removerPosicoes() {
		remove(labelImagem1);
		for (InterfacePosicao a : aPosicoes) {
			if (a != null)
				a.removerBotao(this);
		}
		add(labelImagem1);
		repaint();
	}
	
	//adiciona os botoes ao frame
	private void adicionarPosicoes() {
		remove(labelImagem1);
		for (InterfacePosicao a : aPosicoes) {
			if (a != null)
				a.adicionarBotao(this);
		}
		add(labelImagem1);
		repaint();
	}
	
	//remove o botao placar
	private void removerPlacar() {
		remove(labelImagem1);
		if (placar != null)
			placar.removerPlacar(this);
		add(labelImagem1);
		repaint();
	}
	
	//Remove e adiciona o botao
	private void atualizarPosicoes() {
		remove(labelImagem1);
		for (InterfacePosicao a : aPosicoes) {
			if (a != null) {
				a.removerBotao(this);
				a.adicionarBotao(this);
			}
		}
		add(labelImagem1);
		repaint();
	}
	
	void atualizarPlacar() {
		remove(labelImagem1);
		if (!placar.equals(null)) {
			placar.atualizarPlacar(this);
		}
		add(labelImagem1);
		repaint();
	}
	
	//Remove e adiciona as cartas
	private void atualizarCartas() {
		remove(labelImagem1);
		for (InterfaceCarta a : aCartas) {
			if (a != null) {
				a.removerBotao(this);
				a.adicionarBotao(this);
			}
		}
		add(labelImagem1);
		repaint();
	}
	
	public void receberJogada() {
		iniciarPosicoes();
		atualizarPosicoes();
		atualizarPlacar();
		atualizarCartas();
	}
	
	public void adicionarNomeAdversario(String nome) {
		nomeOutroJogador = nome;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		if (source == iniciar) {
			aJogador.iniciarConversa();
		} else if (source == mudarNome) {
			String novoNome = JOptionPane.showInputDialog("Digite o Nome do Jogador: ");
			jogador.receberNome(novoNome);
			nome.setText(novoNome);
		} else if (source == conectar) {
			aJogador.conectar();
		} else if (source == desconectar) {
			aJogador.desconectar();
		} else if (source == desistir) {
			aJogador.desistir();
		} else {
			boolean ehCarta = false;
			boolean usouCarta = false;
			
			for (InterfaceCarta a : aCartas) {
				if (a.ehBotao(source)) {
					ehCarta = true;
					b = a;
					carta = a.carta;
				}
			}
			
			if (ehCarta && jogador.ehVez()) {
				cartaSelecionada = true;
			} else if (jogador.ehVez() && cartaSelecionada){
				for (InterfacePosicao a : aPosicoes) {
					if (a.ehBotao(source)) {
						if (!a.posicao.estaOcupada() && !a.posicao.estaBloqueada()) {
							cartaSelecionada = false;
							aJogador.utilizarCarta(a.posicao.horizontal, a.posicao.vertical, carta);
							usouCarta = true;
						}
					}
				}
				
				if (usouCarta) {
					if (b != null) {
						b.bCarta.setEnabled(false);
					}
					atualizarCartas();
					atualizarPosicoes();
				}
			}
		}
	}
	
	public String recebeInformacaoServidor() {
		String retorno =  JOptionPane.showInputDialog("Digite o servidor: ", "localhost");
		return retorno;
	};
	public void informarVencedor(String vencedor) {
		JOptionPane.showMessageDialog(null, "O Vencedor � : " + vencedor);
	}


}
