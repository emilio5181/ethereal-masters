import javax.swing.JButton;
import javax.swing.JFrame;

public class InterfacePlacar extends JFrame {
	Placar placar;
	JButton bPlacar = new JButton();
	InterfacePlacar(Placar placar) {
		bPlacar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER); 
		bPlacar.setVerticalAlignment(javax.swing.SwingConstants.TOP); 
		bPlacar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		this.placar = placar;
		bPlacar.setBounds(0,700,200,147);
		bPlacar.setEnabled(false);
	}
	
	public void adicionarPlacar(JFrame j) {
		bPlacar.setText(placar.textoInterface());
		j.add(bPlacar);
	}
	
	public void removerPlacar(JFrame j) {
		j.remove(bPlacar);
	}
	
	public void atualizarPlacar(JFrame j) {
		bPlacar.setText(placar.textoInterface());
	}
}
