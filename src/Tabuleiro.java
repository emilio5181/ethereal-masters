import java.util.Random;
public class Tabuleiro {
	Jogador jogador;
	Placar placar;
	AtorJogador aJogador;
	Posicao [] posicoes;
	String vencedor;
	boolean comecou = false;
	
	Tabuleiro (Jogador jogador, AtorJogador aJogador) {
		this.jogador = jogador;
		this.aJogador = aJogador;
		posicoes = new Posicao [16];
	}

	public boolean partidaEmAndamento () {return comecou;};
	
	
	//Verifica se a partida terminou
	public boolean verificarFimPartida () {
		boolean fim = true;
		int tamanho = posicoes.length;
		for (int i = 0; i < tamanho; i++) {
			if (posicoes[i].estaVazia()) {
				fim = false;
				break;
			}
		}
		return fim;
	};
	
	//verifica se uma posicao x y esta livre para colocar carta
	public boolean verificarPosicao (int horizontal, int vertical) {
		int tamanho = posicoes.length;
		boolean retorno = false;
		for (int i = 0; i < tamanho; i++) {
			if ((posicoes[i].ehPosicao(horizontal, vertical)) && !(posicoes[i].estaBloqueada())) {
				retorno = !(posicoes[i].estaOcupada());
			}
		}
		return retorno;
	};
	
	
	
	//recebe uma jogada Jogada da net games(Boolean para desistencia e as posicoes)
	public void receberJogada(boolean desistencia, Posicao[] posicoes) {
		if (desistencia) {
			jogador.informarGanhou();
			vencedor = jogador.informarCor();
			aJogador.informarVencedor(vencedor);
		} else {
			atualizarPosicoes(posicoes);
			atualizarPlacar();
			boolean fim = verificarFimPartida();
			if (fim) {
				vencedor = avaliarVencedor();
				aJogador.informarVencedor(vencedor);
			} else {
				jogador.mudarJogadorHabilitado();
			}
				
		}
		
	};
	
	//utilizar uma carta na posicao x , y
	public void utilizarCarta(int x, int y, Carta carta) {
		boolean posicao = verificarPosicao(x, y);
		
		if (posicao) {
			Posicao atual = posicaoEm(x, y);
			capturarPosicao(atual, carta);
			
			atualizarPlacar();
			
			boolean fim = verificarFimPartida();
			if (fim) {
				String vencedor = avaliarVencedor();
				aJogador.novaMensagem(false, posicoes);
				aJogador.informarVencedor(vencedor);
			} else {
				jogador.mudarJogadorHabilitado();
				aJogador.novaMensagem(false, posicoes);
			}
			
		}
		
	};
	
	public void atualizarPlacar () {
		placar.reiniciarPontos();
		
		int tamanho = posicoes.length;
		for (int i = 0; i < tamanho; i++ ) {
			String jogador = posicoes[i].jogadorControlando();
			if (!jogador.equals("vazio") && !jogador.equals("bloqueada")) {
				if (jogador.equals("Azul")) {
					placar.pontoPara("Azul");
				} else {
					placar.pontoPara("Vermelho");
				}
			}
		}
		
	};
	
	public String avaliarVencedor () {
		boolean desistencia = jogador.perguntaDesistiu();
		if (desistencia) {
			String cor = jogador.informarCor();
			if (cor.equals("Vermelho")) {
				vencedor = "Azul";
			} else {
				vencedor = "Vermelho";
			}
		} else {
			vencedor = placar.avaliarVencedor();
		}
		
		return vencedor;
	};
	
	public void criarPosicoes() {
		int tamanho = 4;
		int cont = 0;
		int numeroBloqueadas = numeroAleatorio(6, 8);
		if (numeroBloqueadas == 7)
			numeroBloqueadas = 6;
		//numeroBloqueadas = 0;
		int []bloqueadas = new int[numeroBloqueadas];

		//Cria as posicoes
		for (int i = 0; i < tamanho; i ++) {
			for (int j = 0; j < tamanho; j ++) {
				posicoes[cont] = new Posicao(i,j);
				cont++;
			}
		}
		
		//Bloqueia as posicoes
		for (int i = 0; i < numeroBloqueadas; i++) {
			bloqueadas[i] = numeroAleatorioDiferente(0, 15, bloqueadas);
		}
		
		for (int i = 0; i < numeroBloqueadas; i++) {
			posicoes[bloqueadas[i]].bloquear();
		}
		
		//adiciona as posicoes adjascentes
		cont = 0;
		for (int i = 0; i < tamanho; i ++) {
			for (int j = 0; j < tamanho; j++) {
				Posicao [] adjs = new Posicao[8];
				int contAdjs = 0;
				
				//Posicao a Esquerda 0
				if ((cont % 4) == 0) {
					adjs[contAdjs] = null;
					contAdjs++;
				} else {
					adjs[contAdjs] = posicoes[cont - 1];
					contAdjs++;
				}
				
				//Posicao em Cima 1
				if (cont < 4) {
					adjs[contAdjs] = null;
					contAdjs++;
				} else {
					adjs[contAdjs] = posicoes[cont - 4];
					contAdjs++;
				}
				
				//Posicao a Direita 2
				if (((cont - 3) % 4) == 0) {
					adjs[contAdjs] = null;
					contAdjs++;
				} else {
					adjs[contAdjs] = posicoes[cont + 1];
					contAdjs++;
				}
				
				//Posicao em Baixo 3
				if (cont > 11) {
					adjs[contAdjs] = null;
					contAdjs++;
				} else {
					adjs[contAdjs] = posicoes[cont + 4];
					contAdjs++;
				}
				
				//Posicao CimaDireita 4
				if ((((cont - 3) % 4) == 0) || (cont < 4)) {
					adjs[contAdjs] = null;
					contAdjs++;
				} else {
					adjs[contAdjs] = posicoes[cont - 3];
					contAdjs++;
				}
				
				//Posicao BaixoDireita 5
				if ((cont > 11) || (((cont - 3) % 4) == 0)) {
					adjs[contAdjs] = null;
					contAdjs++;
				} else {
					adjs[contAdjs] = posicoes[cont + 5];
					contAdjs++;
				}
				
				
				//Posicao CimaEsquerda 6
				if ( ((cont % 4) == 0) || (cont < 4) ) {
					adjs[contAdjs] = null;
					contAdjs++;
				} else {
					adjs[contAdjs] = posicoes[cont - 5];
					contAdjs++;
				}
				
				//Posicao BaixoEsquerda 7
				if ( ((cont % 4) == 0) || (cont > 11) ) {
					adjs[contAdjs] = null;
					contAdjs++;
				} else {
					adjs[contAdjs] = posicoes[cont + 3];
					contAdjs++;
				}
					
				posicoes[cont].adicionarAdjascentes(adjs);
				cont++;
			}
		}
	};

	public Carta[] criarCartas() {
		Carta [] retorno = new Carta[5];
		String jog = jogador.informarCor();
		String [] elementos = {"magia", "terra" , "agua" , "fogo" , "ar"};
		for (int i = 0; i < 5 ; i++ ) {
			int numDirecao = numeroAleatorio(3,6);
			//numDirecao = 2;
			String [] direcoes = escolheDirecoes(numDirecao);
			int ataqueMin = numeroAleatorio(1,2);
			int ataqueMax = numeroAleatorio(3,4);
			int defesaMin = numeroAleatorio(1,2);
			int defesaMax = numeroAleatorio(3,4);
			retorno [i] = new Carta(ataqueMin, ataqueMax,defesaMin,defesaMax,direcoes,jog,elementos[i]);
		}
		
		return retorno;
	};
	
	private int numeroAleatorio(int min, int max) {
		Random gerador = new Random();
		int retorno;
		int range = max - min + 1;
		retorno = gerador.nextInt(range) + min;
		return retorno;
	}
	
	public boolean contains(int elemento, int[] array) {
		for (int num : array) {
			if (num == elemento) {
				return true;
			}
		}
		return false;
	}
	
	private int numeroAleatorioDiferente(int min, int max, int[] array) {
		int retorno = numeroAleatorio(min, max);
		int whileSafe = 0;
		while (contains(retorno,array) && whileSafe < 20) {
			retorno = numeroAleatorio(min,max);
			whileSafe ++;
		}
		return retorno;
	}
	
	public void receberSolicitacao(boolean iniciar) {
		criarPosicoes();
		Carta [] cartas;
		cartas = criarCartas();
		comecou = true;
		jogador.distribuirCartas(cartas);
		placar = new Placar();
		
		if (iniciar)
			jogador.mudarJogadorHabilitado();
	};

	//possivelmente melhorar (direcoes aleatorias)
	public String [] escolheDirecoes(int numDirecoes) {
		String [] possiveis = {"esquerda","cima","direita","baixo","direitacima","direitabaixo","esquerdacima","esquerdabaixo"};
		String [] direcoes = new String[numDirecoes];
		int [] escolhidos = new int[numDirecoes];
		for (int i = 0; i < numDirecoes ; i++) {
			escolhidos[i] = -1;
		}
		int indice;
		for (int i = 0; i < numDirecoes ; i++) {
			indice = numeroAleatorioDiferente(0,7,escolhidos);
			direcoes[i] = possiveis[indice];
			escolhidos[i] = indice;
		}
		return direcoes;
	};
	
	public Posicao posicaoEm (int horizontal, int vertical ) {
		int tamanho = posicoes.length;
		Posicao retorno = null;
		for (int i = 0; i < tamanho; i++) {
			if (posicoes[i].ehPosicao(horizontal, vertical)) {
				retorno = posicoes[i];
			}
		}
		return retorno;
	};
	
	public void capturarPosicao(Posicao posicao, Carta carta) {
		posicao.capturarPosicao(carta);
		
		Posicao [] inimigas = posicao.posicoesAdjascentesInimigas();
		
		int tamanho = inimigas.length;
		for (int i = 0; i < tamanho; i++) {
			boolean resultado = posicao.atacar(inimigas[i]);
			if (resultado) {
				capturarPosicao(inimigas[i], null);
			}
		}
		
		
		
	};
	
	public void atualizarPosicoes (Posicao [] posicoes) {
		this.posicoes = posicoes;
	};
	
	
	public void desistir() {
		jogador.desistir();comecou = false;
		vencedor = avaliarVencedor();
		aJogador.informarVencedor(vencedor);
		aJogador.novaMensagem(true, posicoes);
	}
	public Placar placar() {
		return placar;
	}
	public void resetar() {
		comecou = false;
		jogador.resetar();
		
	};
	
	
}