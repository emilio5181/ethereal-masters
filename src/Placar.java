
public class Placar {
	int pontosJogadorAzul;
	int pontosJogadorVermelho;
	Placar () {pontosJogadorAzul = 0; pontosJogadorVermelho = 0;}
	
	public String avaliarVencedor() {
		if (pontosJogadorAzul > pontosJogadorVermelho) {
			return "Azul";
		} else {
			return "Vermelho";
		}
	};
	
	public void reiniciarPontos() {pontosJogadorAzul = 0; pontosJogadorVermelho = 0;};
	
	public void pontoPara(String cor) {
		if (cor == "Azul") 
			pontosJogadorAzul ++;
		else
			pontosJogadorVermelho ++;
	};
	
	public String textoInterface() {
		return "<html>" + "Vermelho: " + pontosJogadorVermelho + "<br>" + "Azul:" + pontosJogadorAzul  +"</html>";
	}
}
