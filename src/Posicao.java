public class Posicao implements br.ufsc.inf.leobr.cliente.Jogada {
	boolean bloqueada;
	boolean temCarta;
	Carta carta;
	Posicao [] posicoesAdjascentes;
	int horizontal;
	int vertical;
	
	Posicao(int horizontal, int vertical) {
		bloqueada = false;
		temCarta = false;
		this.horizontal = horizontal;
		this.vertical = vertical;
	}
	
	public void adicionarAdjascentes (Posicao[] adjs) {
		posicoesAdjascentes = adjs;
	}
	
	public boolean ehPosicao(int x, int y) {return x == horizontal && y == vertical;	}
	
	public boolean estaOcupada() {return temCarta;	}
	
	public void capturarPosicao(Carta carta) {
		if (carta == null) {
			(this.carta).mudarJogadorControlando();
		} else {
			temCarta = true;
			this.carta = carta;
		}
	}
	

	
	public Posicao [] posicoesAdjascentesInimigas(){
		int tamanho = posicoesAdjascentes.length;
		Posicao [] inimigas;
		int [] indiceInimigas;
		int cont = 0;
		for (int i = 0; i < tamanho; i ++) {
			if (posicoesAdjascentes[i] != null) {
				if (!(posicoesAdjascentes[i].jogadorControlando().equals(jogadorControlando())) && posicoesAdjascentes[i].temCarta) {
				//if ((posicoesAdjascentes[i].jogadorControlando() != jogadorControlando()) && posicoesAdjascentes[i].temCarta) {
					cont++;
				}
			}
		}
		inimigas = new Posicao[cont];
		indiceInimigas = new int[cont];
		cont = 0;
		
		for (int i = 0; i < tamanho; i ++) {
			if (posicoesAdjascentes[i] != null) {
				if ((!posicoesAdjascentes[i].jogadorControlando().equals(jogadorControlando())) && posicoesAdjascentes[i].temCarta) {
				//if ((posicoesAdjascentes[i].jogadorControlando() != jogadorControlando())  && posicoesAdjascentes[i].temCarta) {
					inimigas[cont] = posicoesAdjascentes[i];
					indiceInimigas[cont] = i;
					cont++;
				}
			}
		}
		
		int contador = 0;
		for (int i = 0; i < cont ; i++) {
			if(carta.podeAtacar(indiceInimigas[i])) {contador++;} else {
				inimigas[i] = null;
			}
		}
		
		Posicao [] retorno = new Posicao[contador];
		contador = 0;
		for (int i = 0; i < cont ; i++) {
			if(inimigas[i] != null) {
				retorno[contador] = inimigas[i];
				contador++;
			}
		}
		
		return retorno;
	}
	
	public boolean atacar(Posicao defensora) {
		System.out.println("Esta acontecendo um ataque");
		int ataque = carta.definirAtaque(defensora.elemento());
		int defesa = (defensora.carta).definirDefesa();
		return ataque > defesa;
		//return true;
	}
	
	private String elemento() {
		return carta.elem();
	}

	public String jogadorControlando() {
		//Se nao tem jogador Controlando "vazio"
		if (temCarta) {
			return carta.jogadorControlando();
		} else if (bloqueada){
			return "bloqueada";
		} else {
			return "vazio";
		}
	}

	public boolean estaBloqueada() {
		return bloqueada;
	}

	public void bloquear() {
		bloqueada = true;
		
	}
	
	public String elementoCarta() {
		if (temCarta)
			return carta.elem();
		else 
			return "vazio";
	}
	
	public String direcoes() {
		if (carta != null) 
			return carta.direcoes();
		else
			return "vazio";
	}
	
	public int [] valores() {
		if (carta != null)
			return carta.retornarValores();
		else
			return null;
	}

	public boolean estaVazia() {
		return jogadorControlando().equals("vazio");
	}


}
