public class AtorJogador {
	Jogador jogador;
	Tabuleiro tab;
	AtorNetGames netGames;
	Interface inter;

	AtorJogador(Interface inter, Jogador jog) {
		super();
		this.tab = tab;
		this.jogador = jog;
		this.inter = inter;
		netGames = new AtorNetGames(this);
	}
	
	public void adicionarTabuleiro(Tabuleiro tab) {this.tab = tab;}
	
	public String recebeInformacaoServidor() {
		return inter.recebeInformacaoServidor();
	};
	
	//Diagrama de sequencia Conectar
	public void conectar() {
			boolean conectado = jogador.estaConectado();
			if (!conectado) {
				String servidor = recebeInformacaoServidor();
				String nome = jogador.informarNome();
				netGames.conectar(nome, servidor);
				jogador.informarConectado();
			}
	}

	//Diagrama de sequencia desconectar
	public void desconectar() {
		if (tab.partidaEmAndamento()) {
			desistir();
			inter.interfaceInicial();
		}
		jogador.desconectar();
		netGames.desconectar();
	}
	
	//Diagrama de sequencia desistir
	public void desistir() {
		tab.desistir();	
		inter.interfaceInicial();
	}
	
	
	//Diagrama de sequencia utilizar carta
	public void utilizarCarta(int horizontal, int vertical, Carta carta) {
		tab.utilizarCarta(horizontal, vertical, carta);
	}

	public void informarVencedor(String vencedor) {
		System.out.println("\n\n\n\n\nNOVA PARTIDA");
		inter.informarVencedor(vencedor);
		tab.resetar();
		inter.interfaceInicial();
		
	}		
	int jogada = 0;
		
	//envia a Jogada para o oponente
	public boolean novaMensagem(boolean desistencia, Posicao [] posicoes) {
		System.out.println("ENVIOU JOGADA : " + jogada++);
		netGames.enviarJogada(desistencia, posicoes);
		inter.atualizarPlacar();
		return true;		
	}
		
		

	//Receber a solicitacao de inicio
	public void iniciarPartidaRede(Integer posicao) {
		inter.adicionarNomeAdversario(netGames.obterNomeAdversario());
		
		if (posicao == 1) {
			jogador.mudarCor("Vermelho");
			tab.receberSolicitacao(true);
			inter.interfaceEmJogo(true);
		} else {
			jogador.mudarCor("Azul");
			tab.receberSolicitacao(false);
			inter.interfaceEmJogo(false);
		}
	}

	//Iniciar Partida
	public void iniciarConversa() {
		netGames.iniciarPartidaRede();
	}
		
		
	//QUANDO � UMA JOGADA
	public void receberPosicoesRede(Posicao[] posicoes) {
		tab.receberJogada(false, posicoes);
		inter.receberJogada();
	}

	//QUANDO O OPONENTE DESISTE
	public void receberDesistenciaRede() {
		tab.receberJogada(true, null);		
	}

}



