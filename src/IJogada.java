public class IJogada implements br.ufsc.inf.leobr.cliente.Jogada {
	private Posicao [] posicoes;
	boolean desistencia;
	public IJogada(boolean desistencia, Posicao[] posicoes) {
		super();
		this.desistencia = desistencia;
		this.posicoes = posicoes;
	}
	
	public Posicao [] getPosicoes () {
		return posicoes;
	}
	
	public boolean getDesistencia () {
		return desistencia;
	}
	
	
	
	
}
