import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


public class InterfacePosicao {
	Posicao posicao;
	JButton bPosicao = new JButton();
	InterfaceCarta carta;
	
	InterfacePosicao(Posicao pos) {
		posicao = pos;
		if (posicao.estaBloqueada()) {
			bPosicao.setEnabled(false);
			bPosicao.setBackground(Color.black);
		} else {
			bPosicao.setEnabled(true);
		}
		bPosicao.setBounds(posicao.horizontal * 150 + 300,175 + 150 * posicao.vertical,100,100);
		//bPosicao.setVisible(true);
	}
	
	public void adicionarBotao(JFrame j) {
		
		if (posicao.carta == null || posicao.estaBloqueada()) {
			j.add(bPosicao);
		} else {
			String elemento = posicao.elementoCarta();
			switch (elemento) {
			case ("terra"):
				bPosicao.setBackground(new Color(65,105,225));
				break;
			case ("ar"):
				bPosicao.setBackground(new Color(147,112,219));
				break;
			case("fogo"):
				bPosicao.setBackground(new Color(205,201,201));
				break;
			case("agua"):
				bPosicao.setBackground(new Color(255,99,71));
				break;
			default:
				bPosicao.setBackground(new Color(184,134,11));
			}
			
			if (posicao.jogadorControlando().equals("Vermelho")) {
				bPosicao.setForeground(Color.RED);
			} else {
				bPosicao.setForeground(Color.BLUE);
			}
			String direcoesAtaque = posicao.direcoes();
			int[] valores = posicao.valores();
			
			if (valores != null && direcoesAtaque != null)
				bPosicao.setText("<html>" + "Ataque: " + valores[0] + "-" + valores[1] + "<br>" + "Defesa: " + valores[2] + "-" + valores[3] + "<br>"+ direcoesAtaque + "</html>");
			
			j.add(bPosicao);
		}
	}
	
	public void adicionarListener(ActionListener l) {
		bPosicao.addActionListener(l);
	}
	
	public void removerBotao(JFrame j) {
		j.remove(bPosicao);
	}

	public boolean ehBotao(Object source) {
		return source == bPosicao;
	}
	
}
