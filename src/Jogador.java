
public class Jogador {
	boolean ehVez;
	Carta [] cartas;
	Tabuleiro tab;
	String cor;
	String nome;
	boolean ganhou = false;
	boolean estaConectado = false;
	boolean desistiu = false;
	boolean emJogo = false;
	
	Jogador() {
		this.nome = "Usuario";
		this.cor = "Vermelho";
		ehVez = false;
	}
	
	public void resetar() {
		ehVez = false;
		ganhou = false;
		desistiu = false;
		emJogo = false;
	}
	
	public void receberNome(String nome) {this.nome = nome;	}
	
	public String informarNome() {return nome;}
	
	public boolean estaConectado() {return estaConectado;}
	
	public void informarConectado() {estaConectado = true;};
	
	public boolean perguntaDesistiu() {return desistiu;};
	
	public void desistir() {desistiu = true;};
	
	public void mudarJogadorHabilitado () {ehVez = !ehVez;};
	
	public void informarGanhou () {ganhou = true;};
	
	public void distribuirCartas (Carta [] cartas) {this.cartas = cartas;};
	
	public Carta [] informarCartas () {return cartas;}
	
	public String informarCor () {return cor;};
	
	public void mudarCor(String cor) {this.cor = cor;}
	
	public boolean ehVez () {return ehVez;}

	public void desconectar() {estaConectado = false;}
}
