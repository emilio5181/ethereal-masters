import java.util.Random;
public class Carta implements br.ufsc.inf.leobr.cliente.Jogada {
	int ataqueMax;
	int ataqueMin;
	int defesaMax;
	int defesaMin;
	String elemento;
	String [] direcoesDeAtaque;
	String jogador;
	
	Carta(int ataqueMin,int ataqueMax,int defesaMin,int defesaMax,String [] direcoes,String jogador,String elemento) {
		this.ataqueMax = ataqueMax;
		this.ataqueMin = ataqueMin;
		this.defesaMax = defesaMax;
		this.defesaMin = defesaMin;
		this.direcoesDeAtaque = direcoes;
		this.jogador = jogador;
		this.elemento = elemento;
	}
	
	public int[] retornarValores() {
		int [] retorno = {ataqueMax, ataqueMin, defesaMax, defesaMin};
		return retorno;
	}
	
	public String elem() {return elemento;}

	public int definirValor(int min, int max) {
		Random gerador = new Random();
		int retorno;
		retorno = gerador.nextInt(max - min + 1) + min;
		return retorno;
	}
	
	public int definirAtaque(String elem) {
		int retorno = definirValor(ataqueMin, ataqueMax);
		if (elemento.equals("magia")) {
			return retorno;
		} else if (elemento.equals("fogo")) {
			if (elem.equals("ar")) {
				return retorno * 2;
			} else
				return retorno;
		} else if (elemento.equals("ar")) {
			if (elem.equals("terra")){
				return retorno * 2;
			}else
				return retorno;
		} else if (elemento.equals("agua")) {
			if (elem.equals("fogo")){
				return retorno * 2;
			}else
				return retorno;
		} else  if (elemento.equals("terra")){
			if (elem.equals("agua")){
				return retorno * 2;
			}else
				return retorno;
		}
		return retorno;
	}
	
	public int definirDefesa() {return definirValor(defesaMin,defesaMax);}
	
	public String jogadorControlando() {return jogador;}
	
	public void mudarJogadorControlando() {
		if (jogador.equals("Vermelho")) {
			jogador = "Azul";
		} else {
			jogador = "Vermelho";
		}
	}

	public boolean contains(String elemento, String[] array) {
		for (String num : array) {
			if (num.equals(elemento)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean podeAtacar(int i) {
		boolean retorno = false;
		switch (i) {
			case 0:
				retorno = contains("esquerda",direcoesDeAtaque);
				break;
			case 1:
				retorno = contains("cima",direcoesDeAtaque);
				break;
			case 2:
				retorno = contains("direita",direcoesDeAtaque);
				break;
			case 3:
				retorno = contains("baixo",direcoesDeAtaque);
				break;
			case 4:
				retorno = contains("direitacima",direcoesDeAtaque);
				break;
			case 5:
				retorno = contains("direitabaixo",direcoesDeAtaque);
				break;
			case 6:
				retorno = contains("esquerdacima",direcoesDeAtaque);
				break;
			case 7:
				retorno = contains("esquerdabaixo",direcoesDeAtaque);
				break;		
			default:
				retorno =  false;
		}
		return retorno;
	}

	public String direcoes() {
		String retorno = "";
		for (String a : direcoesDeAtaque) {
			if (a.equals("cima")) {
				retorno = retorno + "C ";
			} else if (a.equals("baixo")) {
				retorno = retorno + "B ";
			} else if (a.equals("direita")) {
				retorno = retorno + "D ";
			} else if (a.equals("esquerda")) {
				retorno = retorno + "E ";
			}else if (a.equals("esquerdacima")) {
				retorno = retorno + "EC ";
			} else if(a.equals("esquerdabaixo")) {
				retorno = retorno + "EB ";
			} else if (a.equals("direitacima")) {
				retorno = retorno + "DC ";
			} else if (a.equals("direitabaixo")) {
				retorno = retorno + "DB ";
			}
		}
		return retorno;
	}
}
