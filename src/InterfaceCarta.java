import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class InterfaceCarta extends JFrame {
	Carta carta;
	JButton bCarta = new JButton();

	
	InterfaceCarta (int i) {
		bCarta.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER); 
		bCarta.setVerticalAlignment(javax.swing.SwingConstants.TOP); 
		bCarta.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		switch (i) {
		case (5):
			bCarta.setBounds(1050,200,100,100);
			break;
		case (2):
			bCarta.setBounds(900,400,100,100);
			break;
		case (3):
			bCarta.setBounds(1050,400,100,100);
			break;
		case (4):
			bCarta.setBounds(975,600,100,100);
			break;
		default:
			bCarta.setBounds(900,200,100,100);
		}
	}
	
	public void adicionarListener(ActionListener l) {
		bCarta.addActionListener(l);
	}
	
	public void adicionarCarta(Carta carta) {
		bCarta.setEnabled(true);
		String direcoesAtaque = carta.direcoes();
		int[] valores = carta.retornarValores();
		bCarta.setText("<html>" + "Ataque: " + valores[0] + "-" + valores[1] + "<br>" + "Defesa: " + valores[2] + "-" + valores[3] + "<br>"+ direcoesAtaque  +"</html>");
		String elemento = carta.elem();
		switch (elemento) {
			case ("terra"):
				bCarta.setBackground(new Color(65,105,225));
				break;
			case ("ar"):
				bCarta.setBackground(new Color(147,112,219));
				break;
			case("fogo"):
				bCarta.setBackground(new Color(205,201,201));
				break;
			case("agua"):
				bCarta.setBackground(new Color(255,99,71));
				break;
			default:
				bCarta.setBackground(new Color(184,134,11));
		}
		this.carta = carta;
	}
	
	public void adicionarBotao(JFrame j) {
		j.add(bCarta);
	}
	
	public void removerBotao(JFrame j) {
		j.remove(bCarta);
	}
	
	public void removerCarta() {
		bCarta.setEnabled(false);
		//bCarta.setVisible(false);
		bCarta.setText("");
	}

	public boolean ehBotao(Object source) {
		// TODO Auto-generated method stub
		return source == bCarta;
	}
	
	
}
